from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.http import HttpResponse
from django.http import HttpRequest

from .models import Contenido
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

formulario = """
<form action="" method="POST">
    El recurso solicitado no tiene contenido.
    <p>
    Añadir contenido: <input type="text" name="valor">
    <input type="submit" value="Enviar"> 
</form>
"""


# Create your views here.

@csrf_exempt
def get_content(request, llave):
   
    if request.method == "GET":
        try:
            c = Contenido.objects.get(clave=llave)
            respuesta = "El recurso " + c.clave + " contiene " + c.valor \
                        + " y su id es " + str(c.id)
        except Contenido.DoesNotExist:
            respuesta = formulario

    # if POST
    if request.method == "POST":
        if request.user.is_authenticated:
            # cogemos los datos del cuerpo y los almacenamos en la base de datos
            valor = request.POST['valor']
            # creamos nuevo modelo
            c = Contenido(clave=llave, valor=valor)
            # guardamos en la base de datos
            c.save()
            respuesta = "recurso " + "[" + c.clave + "]" + " con valor " + "(" + c.valor + ")" \
                        + " ID = " + str(c.id)
        else:
            respuesta = "Usuario no logueado, logueate para actualizar el recurso" + '<br><br>' \
                        + "Pincha <a href='/login'>aqui</a> para loguearte:"
                        
     # if PUT
    if request.method == "PUT":
        if request.user.is_authenticated:
            # cogemos los datos del cuerpo y los almacenamos en la base de datos
            valor = request.body.decode('utf-8')

            # Si el recurso existe modificamos el valor de este de la siguiente manera:
            try:
                response = Contenido.objects.get(clave=llave).valor
                c = Contenido.objects.get(clave=llave, valor=response).delete()
                c = Contenido(clave=llave, valor=valor)
            # Cuando el recurso no exista creamos nuevo modelo:
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)
            c.save()
            respuesta = "recurso " + "[" + c.clave + "]" + " con valor " + "(" + c.valor + ")" \
                        + " ID = " + str(c.id)
        else:
            respuesta = "No está logueado. Puedes hacerlo pinchando <a href='/login'>aqui</a>"

    # if GET recurso ---> leemos y si no devolvemos formulario para rellenar
    if request.method == "GET":
        try:
            c = Contenido.objects.get(clave=llave)
            respuesta = "El recurso " + c.clave + " contiene " + c.valor \
                        + " y su id es " + str(c.id)
        except Contenido.DoesNotExist:
            respuesta = formulario

    return HttpResponse(respuesta)


def index(request):
    template = loader.get_template('cms_users_put/index.html')
    return HttpResponse(template.render())


def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Logueado como " + request.user.username + "." + '<br>' \
                    + " Aqui podra crear y actualizar recursos."
    else:
        respuesta = "No está logueado. Puedes hacerlo pinchando <a href='/login'>aqui</a>"

    return HttpResponse(respuesta)


def logout_view(request):
    logout(request)
    return redirect("/cms_users_put/")
